# Notebook jupyter en Python autour d'ISIDORE

Espace d'écriture de notebooks Python sous _JupyterLab_ autour de l'API et du SPARQL d'[isidore.science](https://isidore.science).

Il est possible d'exécuter les notes sans installation de Jupyter via [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fspouyllau%2Fisidore-jupyter/master).

Ce projet est terminé en raison de l'arrêt du SPARQL d'ISIDORE ne sept. 2023.

----