# Classifieur de titres d'ouvrage à partir des jeux de données d'ISIDORE.

Notebooks définisant un classifieur de titre à partir des jeux de données d'[ISIDORE](https://isidore.science) en utilisant les méthodes en _deep learning_.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fspouyllau%2Fisidore-jupyter/master)
